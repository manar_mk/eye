import { CropperModule } from './cropper/cropper.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SelectorComponent } from './selector/selector.component';
import { HttpClientModule } from '@angular/common/http';
import { LoaderComponent } from './loader/loader.component';

@NgModule({
  declarations: [
    AppComponent,
    SelectorComponent,
    LoaderComponent
  ],
  imports: [
    BrowserModule, HttpClientModule, CropperModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

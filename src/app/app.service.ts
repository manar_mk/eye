import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';

import { map } from 'rxjs/operators';

import { environment } from './../environments/environment';

import { Resource, ResourceUpdateResult } from './resource';

@Injectable({
  providedIn: 'root'
})
export class AppService {
  url = environment.baseUrl;

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) {}

  async getResource() {
    try {
      const result = await this.http
        .get<Resource>(`${this.url}/get/image`)
        .pipe(
          map(res => {
            return new Resource(res, this.sanitizer);
          })
        )
        .toPromise();
      return result;
    } catch (error) {
      throw this.parseError(error);
    }
  }

  async deleteResource(imageId) {
    try {
      const result = await this.http
        .post<ResourceUpdateResult>(`${this.url}/delete/image`, {
          image_id: imageId
        })
        .toPromise();
      return result;
    } catch (error) {
      throw this.parseError(error);
    }
  }

  async updateResource(formData) {
    try {
      const result = await this.http
        .post<ResourceUpdateResult>(`${this.url}/update/image`, formData)
        .toPromise();
      return result;
    } catch (error) {
      throw this.parseError(error);
    }
  }

  private parseError(errorResponse: HttpErrorResponse) {
    return (
      (errorResponse.error && errorResponse.error.details) ||
      'неизвестная ошибка'
    );
  }
}

import { Component } from '@angular/core';

import { CropperPosition } from './../cropper/interfaces/cropper-position.interface';
import { ImageCroppedEvent } from './../cropper/interfaces/image-cropped-event.interface';

import { AppService } from './../app.service';
import { Resource } from '../resource';

enum STEPS {
  LOAD = 0,
  IMAGE = 1,
  IRIS = 2,
  PUPIL = 3,
  SUBMIT = 4
}

@Component({
  selector: 'app-selector',
  templateUrl: './selector.component.html',
  styleUrls: ['./selector.component.scss']
})
export class SelectorComponent {
  resource: Resource;

  steps = STEPS;
  currentStep = STEPS.LOAD;

  loadedImage;
  cropperInputImage;

  irisCropperInput: CropperPosition;
  pupilCropperInput: CropperPosition;

  error: string;
  leftImages: number;
  isLoading: boolean;

  formData = {
    image_id: null,
    iris_radius: null,
    pupil_radius: null,
    iris_center_x: null,
    iris_center_y: null,
    pupil_center_x: null,
    pupil_center_y: null
  };

  constructor(private appService: AppService) {}

  async getResource() {
    this.error = '';
    this.startLoading();
    try {
      this.resource = await this.appService.getResource();
      this.formData.image_id = this.resource.image_id;
      this.leftImages = this.resource.left_images;
      this.loadedImage = this.resource.getSanitizedImage();
      this.setStep(STEPS.IMAGE);
      this.stopLoading();
    } catch (error) {
      this.error = `Не удалось запросить изображение: ${error}`;
      this.stopLoading();
    }
  }

  async deleteResource() {
    this.error = '';
    this.startLoading();
    try {
      const deleteResult = await this.appService.deleteResource(
        this.formData.image_id
      );
      this.leftImages = deleteResult.left_images;
      this.getResource();
    } catch (error) {
      this.error = `Не удалось удалить изображение: ${error}`;
      this.stopLoading();
    }
  }

  initIris() {
    this.startLoading();
    this.cropperInputImage = this.resource.getBase64();
    this.setStep(STEPS.IRIS);
  }

  initPupil() {
    this.startLoading();
    this.cropperInputImage = this.resource.getBase64();
    this.setStep(STEPS.PUPIL);
  }

  initSubmit() {
    this.startLoading();
    this.setStep(STEPS.SUBMIT);
    this.stopLoading();
  }

  irisCropped(event: ImageCroppedEvent) {
    const radius = event.width / 2;
    this.irisCropperInput = event.cropperPosition;
    this.formData.iris_radius = radius;
    this.formData.iris_center_x = event.cropperPosition.x2 - radius;
    this.formData.iris_center_y = event.cropperPosition.y2 - radius;
  }

  pupilCropped(event: ImageCroppedEvent) {
    const radius = event.width / 2;
    this.pupilCropperInput = event.cropperPosition;
    this.formData.pupil_radius = radius;
    this.formData.pupil_center_x = event.cropperPosition.x2 - radius;
    this.formData.pupil_center_y = event.cropperPosition.y2 - radius;
  }

  async submit() {
    this.startLoading();
    try {
      const updateResult = await this.appService.updateResource(this.formData);
      this.leftImages = updateResult.left_images;
      this.getResource();
    } catch (error) {
      this.error = `Не удалось сохранить данные по изображению: ${error}`;
    }
    this.stopLoading();
  }

  startLoading() {
    this.isLoading = true;
  }

  stopLoading() {
    this.isLoading = false;
  }

  setStep(step: STEPS, event: Event = null) {
    if (event) {
      event.preventDefault();
    }
    this.currentStep = step;
  }
}

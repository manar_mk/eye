import { DomSanitizer } from '@angular/platform-browser';

export class Resource {
  result: string;
  image_id: number;
  image: string;
  left_images: number;

  constructor(res: Resource, private sanitizer: DomSanitizer) {
    this.image_id = res.image_id;
    this.result = res.result;
    this.image = res.image;
    this.left_images = res.left_images;
  }

  getBase64() {
    return `data:image/png;base64, ${this.image}`;
  }

  getSanitizedImage() {
    return this.sanitizer.bypassSecurityTrustResourceUrl(
      `data:image/png;base64, ${this.image}`
    );
  }
}

export class ResourceUpdateResult {
  image_id: number;
  left_images: number;
  result: string;
}
